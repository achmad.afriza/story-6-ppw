from django.shortcuts import render, redirect
from django.contrib import messages
from django.core import validators
from django.urls import reverse

# Create your views here.

def index(request):
    context = {
        'npm': '1906315821',
        'nav': [
            [reverse('homepage:index'), 'Home'],
            ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
            [reverse('challenge:index'), 'Challenge']
        ]
    }

    return render(request, 'challenge.html', context)