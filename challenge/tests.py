from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse, resolve
from .views import index
from .apps import ChallengeConfig
from . import views

# Create your tests here.

class ChallengeTests(TestCase):
    def test_apps(self):
        self.assertEqual(ChallengeConfig.name, 'challenge')
    
    def test_homepage_status_code(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        response = self.client.get(reverse('challenge:index'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('challenge:index'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'challenge.html')

    def test_homepage_contains_correct_html(self):
        response = self.client.get(reverse('challenge:index'))
        self.assertContains(response, "challenge.css")

    def test_homepage_does_not_contain_incorrect_html(self):
        response = self.client.get(reverse('challenge:index'))
        self.assertNotContains(response, "homepage.css")
    
    def test_homepage_uses_correct_view(self):
        func = resolve(reverse('challenge:index'))
        self.assertEqual(views.index, func.func)